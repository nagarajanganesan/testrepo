angular.module('tinMobileWebApp').run(['$templateCache', function($templateCache) {
  'use strict';

  $templateCache.put('app/404.html',
    "<!DOCTYPE html>\r" +
    "\n" +
    "<html lang=\"en\">\r" +
    "\n" +
    "    <head>\r" +
    "\n" +
    "        <meta charset=\"utf-8\">\r" +
    "\n" +
    "        <title>Page Not Found :(</title>\r" +
    "\n" +
    "        <style>\r" +
    "\n" +
    "            ::-moz-selection {\r" +
    "\n" +
    "                background: #b3d4fc;\r" +
    "\n" +
    "                text-shadow: none;\r" +
    "\n" +
    "            }\r" +
    "\n" +
    "\r" +
    "\n" +
    "            ::selection {\r" +
    "\n" +
    "                background: #b3d4fc;\r" +
    "\n" +
    "                text-shadow: none;\r" +
    "\n" +
    "            }\r" +
    "\n" +
    "\r" +
    "\n" +
    "            html {\r" +
    "\n" +
    "                padding: 30px 10px;\r" +
    "\n" +
    "                font-size: 20px;\r" +
    "\n" +
    "                line-height: 1.4;\r" +
    "\n" +
    "                color: #737373;\r" +
    "\n" +
    "                background: #f0f0f0;\r" +
    "\n" +
    "                -webkit-text-size-adjust: 100%;\r" +
    "\n" +
    "                -ms-text-size-adjust: 100%;\r" +
    "\n" +
    "            }\r" +
    "\n" +
    "\r" +
    "\n" +
    "            html,\r" +
    "\n" +
    "            input {\r" +
    "\n" +
    "                font-family: \"Open Sans\", Arial, sans-serif;\r" +
    "\n" +
    "            }\r" +
    "\n" +
    "\r" +
    "\n" +
    "            body {\r" +
    "\n" +
    "                max-width: 500px;\r" +
    "\n" +
    "                _width: 500px;\r" +
    "\n" +
    "                padding: 30px 20px 50px;\r" +
    "\n" +
    "                border: 1px solid #b3b3b3;\r" +
    "\n" +
    "                border-radius: 4px;\r" +
    "\n" +
    "                margin: 0 auto;\r" +
    "\n" +
    "                box-shadow: 0 1px 10px #a7a7a7, inset 0 1px 0 #fff;\r" +
    "\n" +
    "                background: #fcfcfc;\r" +
    "\n" +
    "            }\r" +
    "\n" +
    "\r" +
    "\n" +
    "            h1 {\r" +
    "\n" +
    "                margin: 0 10px;\r" +
    "\n" +
    "                font-size: 50px;\r" +
    "\n" +
    "                text-align: center;\r" +
    "\n" +
    "            }\r" +
    "\n" +
    "\r" +
    "\n" +
    "            h1 span {\r" +
    "\n" +
    "                color: #bbb;\r" +
    "\n" +
    "            }\r" +
    "\n" +
    "\r" +
    "\n" +
    "            h3 {\r" +
    "\n" +
    "                margin: 1.5em 0 0.5em;\r" +
    "\n" +
    "            }\r" +
    "\n" +
    "\r" +
    "\n" +
    "            p {\r" +
    "\n" +
    "                margin: 1em 0;\r" +
    "\n" +
    "            }\r" +
    "\n" +
    "\r" +
    "\n" +
    "            ul {\r" +
    "\n" +
    "                padding: 0 0 0 40px;\r" +
    "\n" +
    "                margin: 1em 0;\r" +
    "\n" +
    "            }\r" +
    "\n" +
    "\r" +
    "\n" +
    "            .container {\r" +
    "\n" +
    "                max-width: 380px;\r" +
    "\n" +
    "                _width: 380px;\r" +
    "\n" +
    "                margin: 0 auto;\r" +
    "\n" +
    "            }\r" +
    "\n" +
    "\r" +
    "\n" +
    "            /* google search */\r" +
    "\n" +
    "\r" +
    "\n" +
    "            #goog-fixurl ul {\r" +
    "\n" +
    "                list-style: none;\r" +
    "\n" +
    "                padding: 0;\r" +
    "\n" +
    "                margin: 0;\r" +
    "\n" +
    "            }\r" +
    "\n" +
    "\r" +
    "\n" +
    "            #goog-fixurl form {\r" +
    "\n" +
    "                margin: 0;\r" +
    "\n" +
    "            }\r" +
    "\n" +
    "\r" +
    "\n" +
    "            #goog-wm-qt,\r" +
    "\n" +
    "            #goog-wm-sb {\r" +
    "\n" +
    "                border: 1px solid #bbb;\r" +
    "\n" +
    "                font-size: 16px;\r" +
    "\n" +
    "                line-height: normal;\r" +
    "\n" +
    "                vertical-align: top;\r" +
    "\n" +
    "                color: #444;\r" +
    "\n" +
    "                border-radius: 2px;\r" +
    "\n" +
    "            }\r" +
    "\n" +
    "\r" +
    "\n" +
    "            #goog-wm-qt {\r" +
    "\n" +
    "                width: 220px;\r" +
    "\n" +
    "                height: 20px;\r" +
    "\n" +
    "                padding: 5px;\r" +
    "\n" +
    "                margin: 5px 10px 0 0;\r" +
    "\n" +
    "                box-shadow: inset 0 1px 1px #ccc;\r" +
    "\n" +
    "            }\r" +
    "\n" +
    "\r" +
    "\n" +
    "            #goog-wm-sb {\r" +
    "\n" +
    "                display: inline-block;\r" +
    "\n" +
    "                height: 32px;\r" +
    "\n" +
    "                padding: 0 10px;\r" +
    "\n" +
    "                margin: 5px 0 0;\r" +
    "\n" +
    "                white-space: nowrap;\r" +
    "\n" +
    "                cursor: pointer;\r" +
    "\n" +
    "                background-color: #f5f5f5;\r" +
    "\n" +
    "                background-image: -webkit-linear-gradient(rgba(255,255,255,0), #f1f1f1);\r" +
    "\n" +
    "                background-image: -moz-linear-gradient(rgba(255,255,255,0), #f1f1f1);\r" +
    "\n" +
    "                background-image: -ms-linear-gradient(rgba(255,255,255,0), #f1f1f1);\r" +
    "\n" +
    "                background-image: -o-linear-gradient(rgba(255,255,255,0), #f1f1f1);\r" +
    "\n" +
    "                -webkit-appearance: none;\r" +
    "\n" +
    "                -moz-appearance: none;\r" +
    "\n" +
    "                appearance: none;\r" +
    "\n" +
    "                *overflow: visible;\r" +
    "\n" +
    "                *display: inline;\r" +
    "\n" +
    "                *zoom: 1;\r" +
    "\n" +
    "            }\r" +
    "\n" +
    "\r" +
    "\n" +
    "            #goog-wm-sb:hover,\r" +
    "\n" +
    "            #goog-wm-sb:focus {\r" +
    "\n" +
    "                border-color: #aaa;\r" +
    "\n" +
    "                box-shadow: 0 1px 1px rgba(0, 0, 0, 0.1);\r" +
    "\n" +
    "                background-color: #f8f8f8;\r" +
    "\n" +
    "            }\r" +
    "\n" +
    "\r" +
    "\n" +
    "            #goog-wm-qt:hover,\r" +
    "\n" +
    "            #goog-wm-qt:focus {\r" +
    "\n" +
    "                border-color: #105cb6;\r" +
    "\n" +
    "                outline: 0;\r" +
    "\n" +
    "                color: #222;\r" +
    "\n" +
    "            }\r" +
    "\n" +
    "\r" +
    "\n" +
    "            input::-moz-focus-inner {\r" +
    "\n" +
    "                padding: 0;\r" +
    "\n" +
    "                border: 0;\r" +
    "\n" +
    "            }\r" +
    "\n" +
    "        </style>\r" +
    "\n" +
    "    </head>\r" +
    "\n" +
    "    <body>\r" +
    "\n" +
    "        <div class=\"container\">\r" +
    "\n" +
    "            <h1>Not found <span>:(</span></h1>\r" +
    "\n" +
    "            <p>Sorry, but the page you were trying to view does not exist.</p>\r" +
    "\n" +
    "            <p>It looks like this was the result of either:</p>\r" +
    "\n" +
    "            <ul>\r" +
    "\n" +
    "                <li>a mistyped address</li>\r" +
    "\n" +
    "                <li>an out-of-date link</li>\r" +
    "\n" +
    "            </ul>\r" +
    "\n" +
    "            <script>\r" +
    "\n" +
    "                var GOOG_FIXURL_LANG = (navigator.language || '').slice(0,2),GOOG_FIXURL_SITE = location.host;\r" +
    "\n" +
    "            </script>\r" +
    "\n" +
    "            <script src=\"http://linkhelp.clients.google.com/tbproxy/lh/wm/fixurl.js\"></script>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "    </body>\r" +
    "\n" +
    "</html>\r" +
    "\n"
  );


  $templateCache.put('app/index.html',
    "<!DOCTYPE html>\r" +
    "\n" +
    "<html>\r" +
    "\n" +
    "  <head>\r" +
    "\n" +
    "    <meta charset=\"utf-8\" />\r" +
    "\n" +
    "    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge,chrome=1\" />\r" +
    "\n" +
    "    <title ng-bind-template=\"{{ pageTitle }}\"></title>\r" +
    "\n" +
    "    <meta name=\"description\" content=\"\" />\r" +
    "\n" +
    "    <meta name=\"fragment\" content=\"!\" />\r" +
    "\n" +
    "    <meta name=\"viewport\" content=\"user-scalable=no, initial-scale=1.0, maximum-scale=1.0, width=device-width\" />\r" +
    "\n" +
    "\r" +
    "\n" +
    "    <link rel=\"apple-touch-icon\" href=\"touch-icon-iphone.png\">\r" +
    "\n" +
    "    <link rel=\"apple-touch-icon\" sizes=\"76x76\" href=\"touch-icon-ipad.png\">\r" +
    "\n" +
    "    <link rel=\"apple-touch-icon\" sizes=\"120x120\" href=\"touch-icon-iphone-retina.png\">\r" +
    "\n" +
    "    <link rel=\"apple-touch-icon\" sizes=\"152x152\" href=\"touch-icon-ipad-retina.png\">\r" +
    "\n" +
    "\r" +
    "\n" +
    "    <!-- Canonical populated from footer - crazy but necessary -->\r" +
    "\n" +
    "    <link rel=\"canonical\" href=\"\" />\r" +
    "\n" +
    "    <base href=\"/\">\r" +
    "\n" +
    "    <link rel=\"stylesheet\" href=\"/styles/main.css\" />\r" +
    "\n" +
    "    <link rel=\"stylesheet\" href=\"http://www.derbytelegraph.co.uk/dyn_css/colours.css\"/>\r" +
    "\n" +
    "    <link rel=\"stylesheet\" href=\"/components/slick-carousel/slick/slick.css\">\r" +
    "\n" +
    "    <link href='http://fonts.googleapis.com/css?family=Open+Sans:700,400' rel='stylesheet' type='text/css'>\r" +
    "\n" +
    "\r" +
    "\n" +
    "    <script type=\"text/javascript\">\r" +
    "\n" +
    "      var googletag = googletag || {};\r" +
    "\n" +
    "      googletag.cmd = googletag.cmd || [];\r" +
    "\n" +
    "      (function() {\r" +
    "\n" +
    "        var gads = document.createElement(\"script\");\r" +
    "\n" +
    "        gads.async = true;\r" +
    "\n" +
    "        gads.type = \"text/javascript\";\r" +
    "\n" +
    "        var useSSL = \"https:\" == document.location.protocol;\r" +
    "\n" +
    "        gads.src = (useSSL ? \"https:\" : \"http:\") +\r" +
    "\n" +
    "                   \"//www.googletagservices.com/tag/js/gpt_mobile.js\";\r" +
    "\n" +
    "        var node = document.getElementsByTagName(\"script\")[0];\r" +
    "\n" +
    "        node.parentNode.insertBefore(gads, node);\r" +
    "\n" +
    "      })();\r" +
    "\n" +
    "    </script>\r" +
    "\n" +
    "\r" +
    "\n" +
    "    <script>\r" +
    "\n" +
    "      // Angulartics code (component)\r" +
    "\n" +
    "      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){\r" +
    "\n" +
    "      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),\r" +
    "\n" +
    "      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)\r" +
    "\n" +
    "      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');\r" +
    "\n" +
    "\r" +
    "\n" +
    "      ga('create', 'UA-42412357-1', document.domain.match(/^(.+)/)[1]);\r" +
    "\n" +
    "\r" +
    "\n" +
    "    </script>\r" +
    "\n" +
    "\r" +
    "\n" +
    "    <script src=\"http://maps.googleapis.com/maps/api/js?key=AIzaSyBYl7QRJxsbG4M5Rvuv7Me7kfqqVqNnYss&amp;sensor=false\"></script>\r" +
    "\n" +
    "\r" +
    "\n" +
    "    <!-- This is for when a user pastes a Brightcove video into an article.\r" +
    "\n" +
    "         TODO (MBLWEB-126): Only pull this in on the article page.  Or only\r" +
    "\n" +
    "                            pull it in if a Brightcove include is detected? -->\r" +
    "\n" +
    "    <script src=\"http://admin.brightcove.com/js/BrightcoveExperiences.js\"></script>\r" +
    "\n" +
    "  </head>\r" +
    "\n" +
    "\r" +
    "\n" +
    "  <body data-status=\"{{ status }}\"\r" +
    "\n" +
    "        id=\"{{ pageId }}\"\r" +
    "\n" +
    "        ng-controller=\"MainCtrl\">\r" +
    "\n" +
    "\r" +
    "\n" +
    "    <script>\r" +
    "\n" +
    "      window.fbAsyncInit = function() {\r" +
    "\n" +
    "        FB.init({\r" +
    "\n" +
    "          appId      : '871490926214230',\r" +
    "\n" +
    "          xfbml      : true,\r" +
    "\n" +
    "          version    : 'v2.0'\r" +
    "\n" +
    "        });\r" +
    "\n" +
    "      };\r" +
    "\n" +
    "\r" +
    "\n" +
    "      (function(d, s, id){\r" +
    "\n" +
    "         var js, fjs = d.getElementsByTagName(s)[0];\r" +
    "\n" +
    "         if (d.getElementById(id)) {return;}\r" +
    "\n" +
    "         js = d.createElement(s); js.id = id;\r" +
    "\n" +
    "         js.src = \"//connect.facebook.net/en_US/sdk.js\";\r" +
    "\n" +
    "         fjs.parentNode.insertBefore(js, fjs);\r" +
    "\n" +
    "       }(document, 'script', 'facebook-jssdk'));\r" +
    "\n" +
    "    </script>\r" +
    "\n" +
    "\r" +
    "\n" +
    "        <div class=\"main-content clearfix\" ng-class=\"{'menu-slideout-open': menuOpen}\">\r" +
    "\n" +
    "          <div class=\"site-alerts\" ng-controller=\"AlertCtrl\">\r" +
    "\n" +
    "            <alert close=\"closeAlert($index, alert.alertType)\"\r" +
    "\n" +
    "                   ng-repeat=\"alert in alerts\"\r" +
    "\n" +
    "                   type=\"alert.type\">\r" +
    "\n" +
    "              <p ng-bind-html=\"alert.msg\"></p>\r" +
    "\n" +
    "            </alert>\r" +
    "\n" +
    "          </div>\r" +
    "\n" +
    "          <div data-ng-animate=\"{enter: 'view-enter'}\"\r" +
    "\n" +
    "               data-ng-hide=\"ignoreHeader\"\r" +
    "\n" +
    "               data-ng-include=\"'/views/includes/header.html'\">\r" +
    "\n" +
    "          </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "        <nt-spinner></nt-spinner>\r" +
    "\n" +
    "\r" +
    "\n" +
    "        <div class=\"container\"\r" +
    "\n" +
    "             ng-view>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "      </div>\r" +
    "\n" +
    "      <nav class=\"main-menu\" ng-class=\"{'menu-slideout-open': menuOpen}\">\r" +
    "\n" +
    "        <div ng-include=\"'/views/includes/main-menu.html'\"></div>\r" +
    "\n" +
    "      </nav>\r" +
    "\n" +
    "\r" +
    "\n" +
    "      <div id=\"fb-root\"></div> <!-- Facebook -->\r" +
    "\n" +
    "      <div id=\"oop1\"></div> <!-- Out-of-page Google ad -->\r" +
    "\n" +
    "\r" +
    "\n" +
    "    <script src=\"//ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js\"></script>\r" +
    "\n" +
    "    <script src=\"//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js\"></script>\r" +
    "\n" +
    "    <script src=\"//ajax.googleapis.com/ajax/libs/angularjs/1.2.24/angular.min.js\"></script>\r" +
    "\n" +
    "    <script src=\"//ajax.googleapis.com/ajax/libs/angularjs/1.2.24/angular-cookies.min.js\"></script>\r" +
    "\n" +
    "    <script src=\"//ajax.googleapis.com/ajax/libs/angularjs/1.2.24/angular-resource.min.js\"></script>\r" +
    "\n" +
    "    <script src=\"//ajax.googleapis.com/ajax/libs/angularjs/1.2.24/angular-route.min.js\"></script>\r" +
    "\n" +
    "    <script src=\"//ajax.googleapis.com/ajax/libs/angularjs/1.2.24/angular-sanitize.min.js\"></script>\r" +
    "\n" +
    "    <script src=\"//ajax.googleapis.com/ajax/libs/angularjs/1.2.24/angular-touch.min.js\"></script>\r" +
    "\n" +
    "    <script src=\"http://cdn.taboolasyndication.com/libtrc/northcliffe-network/loader.js\"></script>\r" +
    "\n" +
    "\r" +
    "\n" +
    "    <!-- build:js scripts/libraries.js -->\r" +
    "\n" +
    "      <script src=\"components/angular-bootstrap/ui-bootstrap-tpls.js\"></script>\r" +
    "\n" +
    "      <script src=\"components/angular-carousel/dist/angular-carousel.js\"></script>\r" +
    "\n" +
    "      <script src='components/lodash/dist/lodash.underscore.js'></script>\r" +
    "\n" +
    "      <script src=\"components/angular-google-maps/dist/angular-google-maps.js\"></script>\r" +
    "\n" +
    "      <script src=\"components/moment/min/moment.min.js\"></script>\r" +
    "\n" +
    "      <script src=\"components/angular-ui-map/ui-map.js\"></script>\r" +
    "\n" +
    "      <script src=\"components/angular-ui-utils/ui-utils.js\"></script>\r" +
    "\n" +
    "      <script src=\"components/angulartics/src/angulartics.js\"></script>\r" +
    "\n" +
    "      <script src=\"components/angulartics/src/angulartics-ga.js\"></script>\r" +
    "\n" +
    "      <script src=\"components/slick-carousel/slick/slick.js\"></script>\r" +
    "\n" +
    "      <script src=\"components/angular-slick/dist/slick.js\"></script>\r" +
    "\n" +
    "      <script src=\"components/canvas-to-blob/js/canvas-to-blob.min.js\"></script>\r" +
    "\n" +
    "      <script src=\"components/exif.js/exif.js\"></script>\r" +
    "\n" +
    "      <script src=\"components/jquery.cookie/jquery.cookie.js\"></script>\r" +
    "\n" +
    "      <script src=\"components/megapix/src/megapix-image.js\"></script>\r" +
    "\n" +
    "      <script src=\"components/angular-promise-tracker/promise-tracker.min.js\"></script>\r" +
    "\n" +
    "      <script src=\"components/within-viewport/withinViewport.js\"></script>\r" +
    "\n" +
    "      <script src=\"components/within-viewport/jquery.withinViewport.js\"></script>\r" +
    "\n" +
    "\r" +
    "\n" +
    "      <script src=\"libraries/jsonscriptrequest/jsr-class.js\"></script>\r" +
    "\n" +
    "    <!-- endbuild -->\r" +
    "\n" +
    "\r" +
    "\n" +
    "    <!-- build:js scripts/scripts.js -->\r" +
    "\n" +
    "      <script src=\"scripts/app.js\"></script>\r" +
    "\n" +
    "      <script src=\"scripts/environment.js\"></script>\r" +
    "\n" +
    "      <script src=\"scripts/functions.js\"></script>\r" +
    "\n" +
    "      <script src=\"scripts/router.js\"></script>\r" +
    "\n" +
    "      <script src=\"scripts/site.js\"></script>\r" +
    "\n" +
    "\r" +
    "\n" +
    "      <script src=\"scripts/controllers/alert.js\"></script>\r" +
    "\n" +
    "      <script src=\"scripts/controllers/articles.js\"></script>\r" +
    "\n" +
    "      <script src=\"scripts/controllers/businesses.js\"></script>\r" +
    "\n" +
    "      <script src=\"scripts/controllers/deals.js\"></script>\r" +
    "\n" +
    "      <script src=\"scripts/controllers/events.js\"></script>\r" +
    "\n" +
    "      <script src=\"scripts/controllers/header.js\"></script>\r" +
    "\n" +
    "      <script src=\"scripts/controllers/homepage.js\"></script>\r" +
    "\n" +
    "      <script src=\"scripts/controllers/main.js\"></script>\r" +
    "\n" +
    "      <script src=\"scripts/controllers/notices.js\"></script>\r" +
    "\n" +
    "      <script src=\"scripts/controllers/photos.js\"></script>\r" +
    "\n" +
    "      <script src=\"scripts/controllers/reportAbuse.js\"></script>\r" +
    "\n" +
    "      <script src=\"scripts/controllers/search.js\"></script>\r" +
    "\n" +
    "      <script src=\"scripts/controllers/staticPages.js\"></script>\r" +
    "\n" +
    "      <script src=\"scripts/controllers/videos.js\"></script>\r" +
    "\n" +
    "      <script src=\"scripts/controllers/weather.js\"></script>\r" +
    "\n" +
    "\r" +
    "\n" +
    "      <script src=\"scripts/directives/ntAddImage.js\"></script>\r" +
    "\n" +
    "      <script src=\"scripts/directives/ntAdSlot.js\"></script>\r" +
    "\n" +
    "      <script src=\"scripts/directives/ntBackButton.js\"></script>\r" +
    "\n" +
    "      <script src=\"scripts/directives/ntBusinessCategories.js\"></script>\r" +
    "\n" +
    "      <script src=\"scripts/directives/ntBusinessSearch.js\"></script>\r" +
    "\n" +
    "      <script src=\"scripts/directives/ntCategoryMenu.js\"></script>\r" +
    "\n" +
    "      <script src=\"scripts/directives/ntChannelSelect.js\"></script>\r" +
    "\n" +
    "      <script src=\"scripts/directives/ntCheckList.js\"></script>\r" +
    "\n" +
    "      <script src=\"scripts/directives/ntCommentsForm.js\"></script>\r" +
    "\n" +
    "      <script src=\"scripts/directives/ntDateCheck.js\"></script>\r" +
    "\n" +
    "      <script src=\"scripts/directives/ntDealItem.js\"></script>\r" +
    "\n" +
    "      <script src=\"scripts/directives/ntEventsBanner.js\"></script>\r" +
    "\n" +
    "      <script src=\"scripts/directives/ntEventSearch.js\"></script>\r" +
    "\n" +
    "      <script src=\"scripts/directives/ntExternalLink.js\"></script>\r" +
    "\n" +
    "      <script src=\"scripts/directives/ntFooter.js\"></script>\r" +
    "\n" +
    "      <script src=\"scripts/directives/ntGallery.js\"></script>\r" +
    "\n" +
    "      <script src=\"scripts/directives/ntHomeItem.js\"></script>\r" +
    "\n" +
    "      <script src=\"scripts/directives/ntHomeSection.js\"></script>\r" +
    "\n" +
    "      <script src=\"scripts/directives/ntImageUpload.js\"></script>\r" +
    "\n" +
    "      <script src=\"scripts/directives/ntIsDisabled.js\"></script>\r" +
    "\n" +
    "      <script src=\"scripts/directives/ntLazyLoad.js\"></script>\r" +
    "\n" +
    "      <script src=\"scripts/directives/ntLoadMore.js\"></script>\r" +
    "\n" +
    "      <script src=\"scripts/directives/ntReportAbuse.js\"></script>\r" +
    "\n" +
    "      <script src=\"scripts/directives/ntReviewsForm.js\"></script>\r" +
    "\n" +
    "      <script src=\"scripts/directives/ntScrollTo.js\"></script>\r" +
    "\n" +
    "      <script src=\"scripts/directives/ntSearchResultItem.js\"></script>\r" +
    "\n" +
    "      <script src=\"scripts/directives/ntShareButtons.js\"></script>\r" +
    "\n" +
    "      <script src=\"scripts/directives/ntSiteSearch.js\"></script>\r" +
    "\n" +
    "      <script src=\"scripts/directives/ntSpinner.js\"></script>\r" +
    "\n" +
    "      <script src=\"scripts/directives/ntTaboola.js\"></script>\r" +
    "\n" +
    "      <script src=\"scripts/directives/ntTemperature.js\"></script>\r" +
    "\n" +
    "      <script src=\"scripts/directives/ntTitleResize.js\"></script>\r" +
    "\n" +
    "      <script src=\"scripts/directives/ntToolbar.js\"></script>\r" +
    "\n" +
    "      <script src=\"scripts/directives/ntWhenScrolled.js\"></script>\r" +
    "\n" +
    "      <script src=\"scripts/directives/ntYouAppi.js\"></script>\r" +
    "\n" +
    "\r" +
    "\n" +
    "      <script src=\"scripts/filters/capitalize.js\"></script>\r" +
    "\n" +
    "      <script src=\"scripts/filters/createPath.js\"></script>\r" +
    "\n" +
    "      <script src=\"scripts/filters/getWeatherIcon.js\"></script>\r" +
    "\n" +
    "      <script src=\"scripts/filters/imageSize.js\"></script>\r" +
    "\n" +
    "      <script src=\"scripts/filters/strip.js\"></script>\r" +
    "\n" +
    "      <script src=\"scripts/filters/toMilliseconds.js\"></script>\r" +
    "\n" +
    "      <script src=\"scripts/filters/truncate.js\"></script>\r" +
    "\n" +
    "      <script src=\"scripts/filters/urlSlash.js\"></script>\r" +
    "\n" +
    "      <script src=\"scripts/filters/withSiteDomain.js\"></script>\r" +
    "\n" +
    "\r" +
    "\n" +
    "      <script src=\"scripts/services/businessCategoryMenu.js\"></script>\r" +
    "\n" +
    "      <script src=\"scripts/services/cache.js\"></script>\r" +
    "\n" +
    "      <script src=\"scripts/services/categoryMenu.js\"></script>\r" +
    "\n" +
    "      <script src=\"scripts/services/day.js\"></script>\r" +
    "\n" +
    "      <script src=\"scripts/services/login.js\"></script>\r" +
    "\n" +
    "      <script src=\"scripts/services/models/article.js\"></script>\r" +
    "\n" +
    "      <script src=\"scripts/services/models/business.js\"></script>\r" +
    "\n" +
    "      <script src=\"scripts/services/models/categories.js\"></script>\r" +
    "\n" +
    "      <script src=\"scripts/services/models/channels.js\"></script>\r" +
    "\n" +
    "      <script src=\"scripts/services/models/comment.js\"></script>\r" +
    "\n" +
    "      <script src=\"scripts/services/models/deal.js\"></script>\r" +
    "\n" +
    "      <script src=\"scripts/services/models/event.js\"></script>\r" +
    "\n" +
    "      <script src=\"scripts/services/models/gallery.js\"></script>\r" +
    "\n" +
    "      <script src=\"scripts/services/models/homepage.js\"></script>\r" +
    "\n" +
    "      <script src=\"scripts/services/models/notice.js\"></script>\r" +
    "\n" +
    "      <script src=\"scripts/services/models/reportAbuse.js\"></script>\r" +
    "\n" +
    "      <script src=\"scripts/services/models/review.js\"></script>\r" +
    "\n" +
    "      <script src=\"scripts/services/models/search.js\"></script>\r" +
    "\n" +
    "      <script src=\"scripts/services/models/weather.js\"></script>\r" +
    "\n" +
    "      <script src=\"scripts/services/models/video.js\"></script>\r" +
    "\n" +
    "      <script src=\"scripts/services/omniture.js\"></script>\r" +
    "\n" +
    "      <script src=\"scripts/services/urlify.js\"></script>\r" +
    "\n" +
    "      <script src=\"scripts/services/user.js\"></script>\r" +
    "\n" +
    "\r" +
    "\n" +
    "      <script src=\"scripts/loader.js\"></script>\r" +
    "\n" +
    "    <!-- endbuild -->\r" +
    "\n" +
    "\r" +
    "\n" +
    "    <script src=\"http://widget.youappi.com/widget-server/plugins/js/jquery.youappi.min.js\"></script>\r" +
    "\n" +
    "\r" +
    "\n" +
    "    <script type=\"text/javascript\">\r" +
    "\n" +
    "      // Crazy Egg\r" +
    "\n" +
    "      setTimeout(function(){var a=document.createElement(\"script\");\r" +
    "\n" +
    "      var b=document.getElementsByTagName(\"script\")[0];\r" +
    "\n" +
    "      a.src=document.location.protocol+\"//dnn506yrbagrg.cloudfront.net/pages/scripts/0011/5371.js?\"+Math.floor(new Date().getTime()/3600000);\r" +
    "\n" +
    "      a.async=true;a.type=\"text/javascript\";b.parentNode.insertBefore(a,b)}, 1);\r" +
    "\n" +
    "    </script>\r" +
    "\n" +
    "  </body>\r" +
    "\n" +
    "</html>\r" +
    "\n"
  );


  $templateCache.put('app/ping.html',
    "\r" +
    "\n" +
    "<!DOCTYPE html>\r" +
    "\n" +
    "<html>\r" +
    "\n" +
    "  <head>\r" +
    "\n" +
    "    <meta charset=\"UTF-8\">\r" +
    "\n" +
    "    <meta name=\"viewport\" content=\"width=620, user-scalable=no\">\r" +
    "\n" +
    "    <link href='http://fonts.googleapis.com/css?family=Droid+Sans:400,700' rel='stylesheet' type='text/css'>\r" +
    "\n" +
    "    <style>\r" +
    "\n" +
    "    html, body {\r" +
    "\n" +
    "      height:100%;\r" +
    "\n" +
    "      padding:0;\r" +
    "\n" +
    "      margin:0;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    body {\r" +
    "\n" +
    "      margin: 0;\r" +
    "\n" +
    "      background: #333;\r" +
    "\n" +
    "      color:white;\r" +
    "\n" +
    "      padding: 10px;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    * {\r" +
    "\n" +
    "      font-family: 'Droid Sans', sans-serif;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    h1 {\r" +
    "\n" +
    "      margin-top: 5px;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    h3 {\r" +
    "\n" +
    "      margin: 30px 0 10px 0;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    .image {\r" +
    "\n" +
    "      width: 400px;\r" +
    "\n" +
    "      height: 200px;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    .image-wide {\r" +
    "\n" +
    "      width: 500px;\r" +
    "\n" +
    "      height: 250px;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    .image li, .image-wide li {\r" +
    "\n" +
    "      background-size: auto;\r" +
    "\n" +
    "      background-position: center center;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    div.layer {\r" +
    "\n" +
    "      background-color:rgba(0, 0, 0, 0.5);\r" +
    "\n" +
    "      padding:5px;\r" +
    "\n" +
    "      font-size:10px;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    .repo {\r" +
    "\n" +
    "      width: 300px;\r" +
    "\n" +
    "      height: 200px;\r" +
    "\n" +
    "\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    .repo li {\r" +
    "\n" +
    "      padding:10px;\r" +
    "\n" +
    "      color:black;\r" +
    "\n" +
    "      background: #bbb;\r" +
    "\n" +
    "      box-sizing: border-box;\r" +
    "\n" +
    "      -moz-box-sizing: border-box;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    div.repo-name {\r" +
    "\n" +
    "      font-size: 18px;\r" +
    "\n" +
    "      font-weight: bold;\r" +
    "\n" +
    "      margin-bottom:10px;\r" +
    "\n" +
    "      text-decoration:underline;\r" +
    "\n" +
    "      -webkit-transition: all 1s ease-in;\r" +
    "\n" +
    "      transition: all 1s ease-in;\r" +
    "\n" +
    "      color:black;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    div.repo-name:hover {\r" +
    "\n" +
    "      color:red;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    ul li {\r" +
    "\n" +
    "      list-style-type: none\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    ul.basic {\r" +
    "\n" +
    "      width:300px;\r" +
    "\n" +
    "      height:200px;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    ul.basic li {\r" +
    "\n" +
    "      text-align:center;\r" +
    "\n" +
    "      padding-top:80px;\r" +
    "\n" +
    "      font-size: 22px;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "    a {\r" +
    "\n" +
    "      color:white;\r" +
    "\n" +
    "    }\r" +
    "\n" +
    "\r" +
    "\n" +
    "    </style>\r" +
    "\n" +
    "  </head>\r" +
    "\n" +
    "  <body ng-app=\"myApp\" ng-controller='demoController'>\r" +
    "\n" +
    "    <a href=\"https://github.com/revolunet/angular-carousel\"><img style=\"position: absolute; top: 0; right: 0; border: 0;\" src=\"https://s3.amazonaws.com/github/ribbons/forkme_right_red_aa0000.png\" alt=\"Fork me on GitHub\"></a>\r" +
    "\n" +
    "    <h1>AngularJS Touch Carousel</h1>\r" +
    "\n" +
    "    <div class='intro'>Transform your ng-repeat in a mobile-friendly carousel just by adding a 'rn-carousel' attribute to your ul/li block, thanks to AngularJS magic :)<br><br>\r" +
    "\n" +
    "    Carousels are data-bound to your ngRepeat collections, can be DOM buffered (good for performance), can cycle over your fixed collection or receive on-demand slides for infinite carousels :)\r" +
    "\n" +
    "    <br><br>\r" +
    "\n" +
    "    Try to swipe these demos with your mouse or your finger :\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "\r" +
    "\n" +
    "    <h3>Simple carousel</h3>\r" +
    "\n" +
    "    A very basic carousel with indicators<br><br>\r" +
    "\n" +
    "    <ul rn-carousel rn-carousel-indicator class=\"image\">\r" +
    "\n" +
    "      <li ng-repeat=\"image in cityImages\" style=\"background-image:url({{ image }});\">\r" +
    "\n" +
    "        <div class=\"layer\" ng-bind-html=\"image\"></div>\r" +
    "\n" +
    "      </li>\r" +
    "\n" +
    "    </ul>\r" +
    "\n" +
    "\r" +
    "\n" +
    "\r" +
    "\n" +
    "\r" +
    "\n" +
    "<!--\r" +
    "\n" +
    "    <h3>Forever carousel</h3>\r" +
    "\n" +
    "    A carousel that cycles forever<br><br>\r" +
    "\n" +
    "    <ul rn-carousel rn-carousel-cycle rn-carousel-indicator class=\"image\">\r" +
    "\n" +
    "      <li data-ng-repeat=\"image in cityImages2\" style=\"background-image:url({{ image }});\">\r" +
    "\n" +
    "        <div class=\"layer\" ng-bind-html-unsafe=\"image\"></div>\r" +
    "\n" +
    "      </li>\r" +
    "\n" +
    "    </ul>\r" +
    "\n" +
    "\r" +
    "\n" +
    "    <h3>Forever buffered carousel</h3>\r" +
    "\n" +
    "    A cycling buffered carousel<br><br>\r" +
    "\n" +
    "    <ul rn-carousel rn-carousel-cycle rn-carousel-buffered class=\"image\">\r" +
    "\n" +
    "      <li ng-repeat=\"image in cityImages2\" style=\"background-image:url({{ image }});\">\r" +
    "\n" +
    "        <div class=\"layer\" ng-bind-html-unsafe=\"image\"></div>\r" +
    "\n" +
    "      </li>\r" +
    "\n" +
    "    </ul>\r" +
    "\n" +
    "-->\r" +
    "\n" +
    "\r" +
    "\n" +
    "\r" +
    "\n" +
    "    <h3 data-ng-bind-html=\"'Discover sports (slide ' + (currentSport + 1) + '/' + sportImages.length + ')'\"></h3>\r" +
    "\n" +
    "    This carousel has two way data-binding, custom controls and is buffered (only 3 slides rendered)<br><br>\r" +
    "\n" +
    "    <ul data-rn-carousel rn-carousel-buffered rn-carousel-index=\"currentSport\" class=\"image\">\r" +
    "\n" +
    "      <li ng-repeat=\"image in sportImages\" style=\"background-image:url({{ image }});\">\r" +
    "\n" +
    "        <div class=\"layer\" ng-bind-html=\"image\"></div>\r" +
    "\n" +
    "      </li>\r" +
    "\n" +
    "    </ul>\r" +
    "\n" +
    "\r" +
    "\n" +
    "    <button ng-click=\"currentSport = currentSport - 1\" ng-disabled=\"currentSport == 0\">prev</button>\r" +
    "\n" +
    "    <button ng-click=\"currentSport = currentSport + 1\" ng-disabled=\"currentSport == sportImages.length - 1\">next</button>\r" +
    "\n" +
    "\r" +
    "\n" +
    "    <h3>Our Public GitHub repos</h3>\r" +
    "\n" +
    "    This carousel has a more advanced template and DOM buffered.<br><br>\r" +
    "\n" +
    "    <ul rn-carousel rn-carousel-buffered class=\"repo\" >\r" +
    "\n" +
    "      <li ng-repeat=\"repo in githubRepos\">\r" +
    "\n" +
    "          <div class=\"repo-name\">{{ repo.name }}</div>\r" +
    "\n" +
    "          {{ repo.description }}<br>\r" +
    "\n" +
    "          <br>\r" +
    "\n" +
    "          <b>watchers</b> : {{ repo.watchers_count }}<br>\r" +
    "\n" +
    "          <b>forks</b> : {{ repo.forks_count }}<br>\r" +
    "\n" +
    "          <b>open issues</b> : {{ repo.open_issues_count }}<br>\r" +
    "\n" +
    "      </li>\r" +
    "\n" +
    "    </ul>\r" +
    "\n" +
    "\r" +
    "\n" +
    "    <h3>Other examples</h3>\r" +
    "\n" +
    "\r" +
    "\n" +
    "<!--    <lI><a href=\"./demo/infinite.html\">Infinite buffered carousel</a> : full screen example for mobile devices. Previous/Next slides are generated on-demand from functions or promises.</li>\r" +
    "\n" +
    "      <br>\r" +
    "\n" +
    "    -->\r" +
    "\n" +
    "    <li><a href=\"./demo/flickr-full/index.html\">Fullscreen Flickr demo (mobile)</a> : demo using Flickr API and a carousel to display Flickr groups feeds</li>\r" +
    "\n" +
    "    <li><a href=\"./demo/infinite.html\">Infinite carousel 1</a> : using rn-carousel-prev and rn-carousel-next</li>\r" +
    "\n" +
    "    <li><a href=\"./demo/infinite2.html\">Infinite carousel 2</a> : using rn-carousel-infinite and prev/next callbacks</li>\r" +
    "\n" +
    "    <li><a href=\"./demo/dynamic.html\">add/remove slides dynamically</a> : using the rn-carousel-watch attribute</li>\r" +
    "\n" +
    "\r" +
    "\n" +
    "\r" +
    "\n" +
    "    <br><br><br>\r" +
    "\n" +
    "    <script src=\"//code.angularjs.org/1.1.5/angular.js\"></script>\r" +
    "\n" +
    "    <script src=\"/libraries/angular-carousel/dist/angular-mobile.js\"></script>\r" +
    "\n" +
    "\r" +
    "\n" +
    "    <!-- include carousel src -->\r" +
    "\n" +
    "    <script src=\"./libraries/angular-carousel/dist/angular-carousel.js\"></script>\r" +
    "\n" +
    "    <link rel=\"stylesheet\" href=\"/styles/main.css\" />\r" +
    "\n" +
    "    <link href='./libraries/angular-carousel/dist/angular-carousel.min.css' rel='stylesheet' type='text/css'>\r" +
    "\n" +
    "\r" +
    "\n" +
    "    <script>\r" +
    "\n" +
    "    /* demo page code */\r" +
    "\n" +
    "    angular.module('myApp', ['angular-carousel'])\r" +
    "\n" +
    "      .controller('demoController', function($scope, $http) {\r" +
    "\n" +
    "        $scope.sportImages = [];\r" +
    "\n" +
    "        $scope.cityImages = [];\r" +
    "\n" +
    "        $scope.cityImages2 = [];\r" +
    "\n" +
    "        $scope.githubRepos = [];\r" +
    "\n" +
    "\r" +
    "\n" +
    "        $scope.currentSport = 0;\r" +
    "\n" +
    "\r" +
    "\n" +
    "        $http.get('https://api.github.com/users/revolunet/repos', {\r" +
    "\n" +
    "          params: {\r" +
    "\n" +
    "            sort: 'updated',\r" +
    "\n" +
    "            order:'desc'\r" +
    "\n" +
    "          }\r" +
    "\n" +
    "        }).success(function(data) {\r" +
    "\n" +
    "          $scope.githubRepos = data;\r" +
    "\n" +
    "        }).error(function() {\r" +
    "\n" +
    "          alert('cannot fetch github API');\r" +
    "\n" +
    "        })\r" +
    "\n" +
    "\r" +
    "\n" +
    "        for (var i=0; i<10; i++) {\r" +
    "\n" +
    "          for (var j=0; j<10; j++) {\r" +
    "\n" +
    "            $scope.sportImages.push('http://lorempixel.com/400/200/sports/' + j + '/');\r" +
    "\n" +
    "          }\r" +
    "\n" +
    "          $scope.cityImages.push('http://lorempixel.com/400/200/city/' + i + '/');\r" +
    "\n" +
    "          if (i<5) $scope.cityImages2.push('http://lorempixel.com/400/200/city/' + i + '/');\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "\r" +
    "\n" +
    "        // infinite carousel stuff\r" +
    "\n" +
    "        var currentDay = (new Date()),\r" +
    "\n" +
    "            colors = ['#339966', '#336699', '#cc9933', '#cc6633', '#cc3366', '#66cc33'];\r" +
    "\n" +
    "\r" +
    "\n" +
    "        function addPage() {\r" +
    "\n" +
    "          // generate a single page, with color and a new date\r" +
    "\n" +
    "          var colorIndex = $scope.pages.length % colors.length;\r" +
    "\n" +
    "          $scope.pages.push({\r" +
    "\n" +
    "            bg: colors[colorIndex],\r" +
    "\n" +
    "            date: currentDay\r" +
    "\n" +
    "          })\r" +
    "\n" +
    "          currentDay = new Date(currentDay.getTime() + 60*60*24*1000);\r" +
    "\n" +
    "        }\r" +
    "\n" +
    "\r" +
    "\n" +
    "        // add some initial pages\r" +
    "\n" +
    "        $scope.pages = [];\r" +
    "\n" +
    "        addPage();\r" +
    "\n" +
    "        addPage();\r" +
    "\n" +
    "        addPage();\r" +
    "\n" +
    "        addPage();\r" +
    "\n" +
    "        addPage();\r" +
    "\n" +
    "\r" +
    "\n" +
    "        $scope.generateSlides = function() {\r" +
    "\n" +
    "          // callback called when carousel reach the penultimate slide\r" +
    "\n" +
    "          // add some slides (could be one)\r" +
    "\n" +
    "          addPage();\r" +
    "\n" +
    "          addPage();\r" +
    "\n" +
    "          addPage();\r" +
    "\n" +
    "        };\r" +
    "\n" +
    "\r" +
    "\n" +
    "      })\r" +
    "\n" +
    "\r" +
    "\n" +
    "    </script>\r" +
    "\n" +
    "  </body>\r" +
    "\n" +
    "</html>\r" +
    "\n"
  );

}]);
